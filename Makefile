.PHONY: help build-all tag-all push-all refresh-all release-all

OWNER := europeanspallationsource
GIT_TAG := $(shell git describe --always)
IMAGES := base-notebook \
	epics-notebook \
	python-notebook \
	julia-notebook \
	openxal-notebook
ALL_IMAGES := $(IMAGES) notebook


help:
# http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo "ics-docker-jupyter-notebook"
	@echo "==========================="
	@echo "Replace % with a stack directory name (e.g., make build/python-notebook)"
	@echo
	@grep -E '^[a-zA-Z0-9_%/-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build/%: ## build the latest image for a stack
	docker build -t $(OWNER)/$(notdir $@):latest ./$(notdir $@)

build-all: $(IMAGES:%=build/%) ## build all stacks
	docker tag $(OWNER)/openxal-notebook:latest $(OWNER)/notebook:latest

tag/%: ## tag the latest stack image with the git tag
	docker tag $(OWNER)/$(notdir $@):latest $(OWNER)/$(notdir $@):$(GIT_TAG)

tag-all: $(ALL_IMAGES:%=tag/%) ## tag all stacks

push/%: ## push the latest and git tag for a stack to Docker Hub
	docker push $(OWNER)/$(notdir $@):$(GIT_TAG)
	docker push $(OWNER)/$(notdir $@):latest

push-all: $(ALL_IMAGES:%=push/%) ## push all stacks

clean/%: ## remove the image with git tag
	-docker rmi $(OWNER)/$(notdir $@):$(GIT_TAG)

clean-all: $(ALL_IMAGES:%=clean/%) ## clean all stacks

refresh/%: ## pull the latest image from Docker Hub for a stack
# skip if error: a stack might not be on dockerhub yet
	-docker pull $(OWNER)/$(notdir $@):latest

refresh-all: $(ALL_IMAGES:%=refresh/%) ## refresh all stacks and centos
	docker pull centos:7

release-all: refresh-all \
	build-all \
	tag-all \
	push-all
release-all: ## build, tag, and push all stacks
