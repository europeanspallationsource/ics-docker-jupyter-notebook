Repository to build ESS jupyter notebook image
==============================================

**DEPRECATED**
Project moved to GitLab: https://gitlab.esss.lu.se/ics-docker/notebook

The Docker_ image `europeanspallationsource/notebook` is built on top of several images:

- base-notebook (base notebook image with Miniconda Python 3.x)
- epics-notebook (add EPICS libraries)
- python-notebook (add Python 3 and Python 2 packages)
- julia-notebook (add Julia)
- openxal-notebook (add OpenXAL)

The notebook image is identical to the openxal-notebook image (it contains everything).


Usage
-----

After updating this repository, you should tag it::

    $ git tag -a 0.4.0

To be able to push the image to Docker Hub, you first have to login::

    $ docker login

You can then run::

    $ make release-all

This will create and push the images:

  - europeanspallationsource/base-notebook
  - europeanspallationsource/epics-notebook
  - europeanspallationsource/python-notebook
  - europeanspallationsource/julia-notebook
  - europeanspallationsource/openxal-notebook
  - europeanspallationsource/notebook

The image is tagged with both `latest` and the current `git tag`.

The images are built automatically by jenkins when pushing to bitbucket.


.. _Docker: https://www.docker.com
