# Configuration file for ipython.

## lines of code to run at IPython startup.
c.InteractiveShellApp.exec_lines = [
    'OPENXAL_PATH = "/opt/OpenXAL"',
    'OPENXAL_VERSION = "1.2.1"',
    'OPENXAL_JAR = "/opt/openxal-1.2.1/lib/openxal.library-1.2.1.jar"'
]
