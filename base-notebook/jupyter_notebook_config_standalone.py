# Configuration file for jupyter-notebook in standalone mode.
# File is not named jupyter_notebook_config.py so that
# it is not read when using jupyterhub.
# It shall be passed as argument to be used.

## (bytes/sec) Maximum rate at which messages can be sent on iopub before they
#  are limited.
c.NotebookApp.iopub_data_rate_limit = 0

## The IP address the notebook server will listen on.
c.NotebookApp.ip = '0.0.0.0'

## The directory to use for notebooks and kernels.
c.NotebookApp.notebook_dir = '/home/conda/notebooks'

## The directories to use for examples
c.Examples.reviewed_example_dir = '/opt/jupyter/examples/reviewed'
c.Examples.unreviewed_example_dir = '/opt/jupyter/examples/unreviewed'

## Whether to open in a browser after starting. The specific browser used is
#  platform dependent and determined by the python standard library `webbrowser`
#  module, unless it is overridden using the --browser (NotebookApp.browser)
#  configuration option.
c.NotebookApp.open_browser = False

## The port the notebook server will listen on.
c.NotebookApp.port = 8888
