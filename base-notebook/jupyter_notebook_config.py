# Default configuration file for notebook used by JupyterHub.

## (bytes/sec) Maximum rate at which messages can be sent on iopub before they
#  are limited.
c.NotebookApp.iopub_data_rate_limit = 0

## The directories to use for examples
c.Examples.reviewed_example_dir = '/opt/jupyter/examples/reviewed'
c.Examples.unreviewed_example_dir = '/opt/jupyter/examples/unreviewed'
